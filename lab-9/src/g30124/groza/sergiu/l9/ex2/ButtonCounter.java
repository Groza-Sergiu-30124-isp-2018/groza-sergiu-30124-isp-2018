package g30124.groza.sergiu.l9.ex2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class ButtonCounter extends JFrame {
    JTextArea tArea;
    JLabel counter;
    JTextField tcounter;
    JButton increment;

    ButtonCounter(){


        setTitle("Incrementator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        tcounter = new JTextField();
        tcounter.setBounds(70,50,width, height);

        increment = new JButton("Increment");
        increment.setBounds(70,100,width, height);

        increment.addActionListener(new TratareButonIncrement());

        add(tcounter);add(increment);

    }

    public static void main(String[] args) {
        new ButtonCounter();
    }


class TratareButonIncrement implements ActionListener{
    int counter=0;
    public void actionPerformed(ActionEvent e) {

        ButtonCounter.this.tcounter.setText("Counter: "+ counter);
        counter++;
    }
}
}