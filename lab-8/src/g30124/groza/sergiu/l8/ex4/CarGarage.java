package g30124.groza.sergiu.l8.ex4;

import java.io.*;

public class CarGarage {

    Car createCar(String model,int price)
    {
        Car c= new Car(model,price);
        return c;
    }

    void addCar(Car c,String garage) throws IOException, ClassNotFoundException
    {
        ObjectOutputStream o= new ObjectOutputStream(new FileOutputStream(garage));
        o.writeObject(c);
        System.out.println("Car added!");
    }

    Car showCar(String garage) throws IOException, ClassNotFoundException {
        ObjectInputStream o = new ObjectInputStream(new FileInputStream(garage));
        Car c = (Car)o.readObject();
        System.out.println(c.toString());
        return c;
    }

    public static void main(String [] args ) throws Exception
    {
        CarGarage g=new CarGarage();

        Car a = g.createCar("BMW",10000);
        Car b = g.createCar("Audi",9000);

        g.addCar(a,"garage1.txt");
        g.addCar(b,"garage2.txt");

        Car x = g.showCar("garage1.txt");
        Car y = g.showCar("garage2.txt");

        System.out.println(x);
        System.out.println(y);
    }
}
