package g30124.groza.sergiu.l8.ex4;

import java.io.Serializable;

public class Car implements Serializable{
    String model;
    int price;

    public Car(String model, int price) {
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }
}
