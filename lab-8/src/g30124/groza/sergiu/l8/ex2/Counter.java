package g30124.groza.sergiu.l8.ex2;

import java.io.*;
import java.util.Scanner;



public class Counter {

    public static char c;

    public static void Leter() throws IOException {
        System.out.println("Introduceti litera dorita:");
        c = (char) System.in.read();
    }

    public static void main(String [] args) throws IOException
    {

        File f = new File("date.txt");
        BufferedReader bf= new BufferedReader(new FileReader(f));
        Leter();
        String l;
        int count=0;
        while ((l=bf.readLine())!=null)
        {
            int l_count=l.length();
            for(int i=0;i<l_count;i++)
                if (l.charAt(i)==c)
                    count++;
        }
        System.out.println("Litera "+c+" apare de "+count+" ori");
    }
}
