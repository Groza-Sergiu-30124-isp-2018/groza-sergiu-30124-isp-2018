package g30124.groza.sergiu.l8.ex1;

public class NumberExeption extends Exception{
    int n;
    public NumberExeption(int n,String msg)
    {
        super(msg);
        this.n=n;
    }

    int getN(){
        return n;
    }
}
