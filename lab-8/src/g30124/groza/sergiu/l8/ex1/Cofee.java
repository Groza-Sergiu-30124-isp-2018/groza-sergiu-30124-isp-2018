package g30124.groza.sergiu.l8.ex1;

class Cofee{
    private int temp;
    private int conc;
    private int number;

    Cofee(int t,int c, int n){temp = t;conc = c;number=n;}
    int getTemp(){return temp;}
    public int getNumber() { return number; }
    int getConc(){return conc;}

    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"cofee number="+number+"]";}
}
