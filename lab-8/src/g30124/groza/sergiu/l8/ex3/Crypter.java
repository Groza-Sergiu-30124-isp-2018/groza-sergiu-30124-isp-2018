package g30124.groza.sergiu.l8.ex3;

import java.io.*;
import java.util.Scanner;

public class Crypter {

    public static void Encription(String fileName) throws IOException {

        File fin = new File(fileName + ".txt");
        BufferedReader bfin = new BufferedReader(new FileReader(fin));

        File fout = new File(fileName + ".enc.txt");
        BufferedWriter bfout = new BufferedWriter(new FileWriter(fout));

        String line;

        while ((line = bfin.readLine()) != null) {
            int line_count = line.length();
            for (int i = 0; i < line_count; i++) {
                char c = line.charAt(i);
                c++;
                bfout.write(c);
            }
            bfout.newLine();
        }
        bfout.close();
    }
    public static void Dencription(String fileName) throws IOException {

        File fin = new File(fileName + ".enc.txt");
        BufferedReader bfin = new BufferedReader(new FileReader(fin));

        File fout = new File(fileName + ".dec.txt");
        BufferedWriter bfout = new BufferedWriter(new FileWriter(fout));

        String line;

        while ((line = bfin.readLine()) != null) {
            int line_count = line.length();
            for (int i = 0; i < line_count; i++) {
                char c = line.charAt(i);
                c--;
                bfout.write(c);
            }
            bfout.newLine();
        }
        bfout.close();
    }


    public static void main(String[] args) throws IOException {
    Scanner s= new Scanner(System.in);

    System.out.println("Enter the name of the file:");
    String file=s.nextLine();
    System.out.println("Enter:\n0 for Encription!  \n1 for Decription!\n2 for EXIT!");
    int choice=s.nextInt();
        while (choice<2)
        {
            if (choice==0)
            {
                Encription(file);
                System.out.println("Encrypted!");
            }
            else if (choice==1)
            {
                Dencription(file);
                System.out.println("Decrypted!");
            }
            choice=s.nextInt();
        }


    }

}
