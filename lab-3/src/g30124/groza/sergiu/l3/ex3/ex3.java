package g30124.groza.sergiu.l3.ex3;

import becker.robots.*;

public class ex3
{
    public static void main(String[] args)
    {

        City prague = new City();
        Robot karel = new Robot(prague, 1, 1, Direction.NORTH);


        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
    }
}
