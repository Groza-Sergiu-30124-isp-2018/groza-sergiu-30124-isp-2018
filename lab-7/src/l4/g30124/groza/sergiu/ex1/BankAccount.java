package l4.g30124.groza.sergiu.ex1;

import java.util.Objects;
import java.util.Comparator;

public class BankAccount implements Comparable<BankAccount> {

    private String owner;
    private double balance;


    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {

        return balance;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }

    @Override
    public int compareTo(BankAccount o) {
        return  (int)(balance-o.balance);
    }

    public static Comparator<BankAccount> BankAccountComparator=new Comparator<BankAccount>(){
        public int compare(BankAccount b1,BankAccount b2)
        {
            String bankName1=b1.getOwner().toUpperCase();
            String bankName2=b2.getOwner().toUpperCase();

            return bankName1.compareTo(bankName2);
        }

    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return that.owner == owner && Double.compare(that.balance, balance) == 0 &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount " +
                "owner:'" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }


}