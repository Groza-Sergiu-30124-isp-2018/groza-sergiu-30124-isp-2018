package l4.g30124.groza.sergiu.ex2;


import l4.g30124.groza.sergiu.ex1.BankAccount;

import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;


public class Bank {
    ArrayList<BankAccount> accounts=new ArrayList<>();

    public void addAccount(String owner,double balance)
    {
        BankAccount b= new BankAccount(owner,balance);
        accounts.add(b);

    }


    public void sortList()
    {
        Collections.sort(accounts);
    }


    public void printAccounts()
    {
        if(accounts!=null)
        {
            sortList();
            for(BankAccount b : accounts)
            {
                System.out.println(b.toString());
            }
        }
    }

    public void printAccounts(double minBalance,double maxBalance)
    {   sortList();
        for(BankAccount b:accounts)
        {
            if(b.getBalance() > minBalance && b.getBalance() < maxBalance)
                System.out.println(b.toString());
        }
    }
    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator=accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount b= iterator.next();
            if(b.getOwner().equals(owner))
                return b;

        }
        return null;
    }

    public ArrayList<BankAccount> getAccounts() {
        return accounts;
    }

}
