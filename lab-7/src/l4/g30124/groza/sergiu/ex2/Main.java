package l4.g30124.groza.sergiu.ex2;

import l4.g30124.groza.sergiu.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        Bank bank=new Bank();

        bank.addAccount("Sergiu",20.2);
        bank.addAccount("Marius",201);
        bank.addAccount("Barbu",140);
        bank.printAccounts();
        System.out.println("-----");
        //bank.printAccounts(20,300);

        ArrayList<BankAccount> bankAccounts= bank.getAccounts();

        Collections.sort(bankAccounts,BankAccount.BankAccountComparator);
        for(BankAccount b:bankAccounts)
            System.out.println(b.toString());

    }
}
