package l4.g30124.groza.sergiu.ex4;


public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return " Definition "+ description;
    }
}