package g30124.groza.sergiu.l5.ex3;

public abstract class Sensor {
    private String location;
    public abstract int readValue();

    public String getLocation()
    {
        return location;
    }
}
