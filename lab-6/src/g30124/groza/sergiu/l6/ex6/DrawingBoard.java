package g30124.groza.sergiu.l6.ex6;

import g30124.groza.sergiu.l6.ex5.Pyramid;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard extends JFrame {

    Fractals[] fractals = new Fractals[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(1000, 1000);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addFractals(Fractals f1) {
        for (int i = 0; i < fractals.length; i++) {
            if (fractals[i] == null) {
                fractals[i] = f1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g) {
        for (int i = 0; i <fractals.length; i++) {
            if (fractals[i] != null) {
                //fractals[i].drawCircle(g, fractals[i].getX(), fractals[i].getY(), fractals[i].getRadius());
                //fractals[i].drawVortex(g, fractals[i].getX(), fractals[i].getY(), fractals[i].getRadius());
                //fractals[i].drawCircleLine(g, fractals[i].getX(), fractals[i].getY(), fractals[i].getRadius());
                fractals[i].drawMileu(g, fractals[i].getX(), fractals[i].getY(), fractals[i].getRadius());
            }
        }
    }

}
