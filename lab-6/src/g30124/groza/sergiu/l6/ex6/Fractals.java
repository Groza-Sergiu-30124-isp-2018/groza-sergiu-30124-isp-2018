package g30124.groza.sergiu.l6.ex6;

import java.awt.*;
import processing.opengl.*;
import processing.core.*;

public class Fractals {
    private int x,y,radius;
    private Graphics g;

    public Fractals(int x, int y,int radius)
    {
        this.x=x;
        this.y=y;
        this.radius=radius;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }


    public int getRadius()
    {
        return radius;
    }

    void drawCircle(Graphics g,int x, int y, int radius) {
        g.drawOval(x, y, radius, radius);
        if(radius > 8) {
            drawCircle(g,x + radius/2, y, radius/2);
            drawCircle(g,x - radius/2, y, radius/2);
            drawCircle(g,x, y + radius/2, radius/2);
            drawCircle(g,x, y - radius/2, radius/2);
        }
    }

    void drawVortex(Graphics g, int x, int y, int radius)
    {
        drawCenteredCircle(g,x,y,radius);
        if(radius>50)
        {

            radius*=0.75;
            drawVortex(g,x,y,radius);
    }
    }

    void drawCircleLine(Graphics g, int x ,int y , int radius)
    {
        drawCenteredCircle(g,x,y,radius);
        if(radius>2)
        {
            drawCircleLine(g,x-radius/2,y,radius/2);
            drawCircleLine(g,x+radius/2,y,radius/2);
        }

    }

    void drawMileu(Graphics g , int x, int y, int radius)
    {
        drawCenteredCircle(g,x,y,radius);
        if(radius<1000)
        {
            drawCircleLine(g,x+radius/2,y,radius/2);
            drawCircleLine(g,x-radius/2,y,radius/2);
            drawCircleLine(g,x,y-radius/2,radius/2);
            drawCircleLine(g,x,y+radius/2,radius/2);
        }

    }

    void drawCenteredCircle(Graphics g, int x, int y, int radius)
    {
        x-=radius/2;
        y-=radius/2;
        g.drawOval(x,y,radius,radius);
    }
}
