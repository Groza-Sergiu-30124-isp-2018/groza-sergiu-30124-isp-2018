package g30124.groza.sergiu.l6.ex3;

import g30124.groza.sergiu.l6.ex1.Shape;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int x,y;

    public Rectangle(Color color, int length,int x,int y, String id,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;

    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length/2);
        if(getFill()==true)
            g.fillRect(getX(),getY(),length,length/2);
    }
}