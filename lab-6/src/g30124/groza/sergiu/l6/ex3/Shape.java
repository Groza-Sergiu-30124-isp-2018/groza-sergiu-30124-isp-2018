package g30124.groza.sergiu.l6.ex3;

import java.awt.*;

public interface Shape {
    void draw(Graphics g);
}
