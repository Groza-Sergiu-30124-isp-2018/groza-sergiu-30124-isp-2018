package g30124.groza.sergiu.l6.ex5;

import java.awt.*;

public class Pyramid {
    private int hight,width;
    private Color color;
    private int x,y;

    public Pyramid(Color color, int hight, int width, int x, int y)
    {
        this.color = color;
        this.width = width;
        this.hight = hight;
        this.x = x;
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getHight()
    {
        return hight;
    }

    public int getWidth()
    {
        return width;
    }

    public void draw(Graphics g) {

        g.setColor(getColor());
        g.drawRect(getX(),getY(),getWidth(),getHight());

    }


}
