package g30124.groza.sergiu.l6.ex5;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static int getBaseLength(int n)
    {

        int x=(int)((-1+Math.sqrt(1+8*n))/2);
        return x;
    }

    public static void main(String [] args)
    {
        int i,j, n, width=40,hight=20,level=0,indentation=0;



        Scanner input =new Scanner(System.in);
        System.out.println("Enter the total number of bricks:");

        n=input.nextInt();
        int base= getBaseLength(n);
        int extra=n-(base*(base+1)/2);

        DrawingBoard b1 = new DrawingBoard();
        Pyramid p1;

        for(i=base;i>=1;i--)
        {
            indentation=0;
            for(j=base;j>=base-i+1;j--)
            {
                p1=new Pyramid(Color.BLUE,hight,width,320+indentation*width+level+100,340+hight*i);
                b1.addPyramid(p1);
                indentation++;
            }
            level+=width/2;
        }


        // cele cu rosu puse sunt caramizile suplimentare ce raman in urma contruirii piramide, in caz ca este
        //necesara utilizarea tuturor caramizilor, cele ramase vor fi puse sub forma de turn in varf


                for (i = extra; i > 0; i--) {
                    p1 = new Pyramid(Color.RED, hight, width, 400 + level, 340 - hight * (extra - i));
                    b1.addPyramid(p1);
                }
            }

}
