package g30124.groza.sergiu.l6.ex5;

import g30124.groza.sergiu.l6.ex1.Shape;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard extends JFrame {

    Pyramid[] pyramids = new Pyramid[100];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(1000, 1000);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addPyramid(Pyramid p1) {
        for (int i = 0; i < pyramids.length; i++) {
            if (pyramids[i] == null) {
                pyramids[i] = p1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g) {
        for (int i = 0; i < pyramids.length; i++) {
            if (pyramids[i] != null)
                pyramids[i].draw(g);
        }
    }

}
