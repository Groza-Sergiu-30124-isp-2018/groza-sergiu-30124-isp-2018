package g30124.groza.sergiu.l6.ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x,y;
    public String id;
    boolean fill;

    public Shape(Color color,int x,int y,String id,Boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public void setId(String id)
    {
        this.id=id;
    }


    public Color getColor() {
        return color;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
    public String getId()
    {
        return id;
    }
    public boolean getFill()
    {
        return fill;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
