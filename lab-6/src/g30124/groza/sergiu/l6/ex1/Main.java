package g30124.groza.sergiu.l6.ex1;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,50,"s1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,50,100,"s2",true);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,100,50,50,"s3",false);
        b1.addShape(s3);
        b1.deleteShape("2");

    }
}
