package g30124.groza.sergiu.l4.ex8;

public class Shape {
    private String color="red";
    private boolean filled=true;
    public Shape()
    {
        color="green";
        filled=true;
    }
    public Shape(String color,boolean filled)
    {
        this.filled=filled;
        this.color=color;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }


    public String toString() {
        if(filled)
            return "A Shape with color of "
                    + color + " and filled";
        else
            return "A Shape with color of "
                    + color + " and Not filled";


    }
}
