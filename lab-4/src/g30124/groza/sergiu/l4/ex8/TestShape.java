package g30124.groza.sergiu.l4.ex8;


import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class TestShape {
    private Shape shape;
    private Shape shape1;
    private Circle circle;
    private Square square;
    private  Rectangle rectangle;
    private Square square1;
    @Before
    public void setUp()
    {   shape1=new Shape("black",false);
        shape=new Shape();
        circle=new Circle(2.5,"green",true);
        rectangle=new Rectangle("white",false,2,3);
        square=new Square(4,"red",false);
        square1=new Square();

    }
    @Test
    public void shouldPrintInfos()
    {
        assertEquals(shape1.toString(),"A Shape with color of black and Not filled");
        assertEquals(shape.toString(),"A Shape with color of green and filled");
        assertEquals(circle.toString(),"A Circle with radius=2.5 which is sublass of A Shape with color of green and filled");
        assertEquals(square.toString(),"A Square with side=4.0 ,which is a subclass of A Rectangle with width=4.0 and length=4.0 , which is a subclass of A Shape with color of red and Not filled");
    }
    @Test
    public void shouldGetArea()
    {
        assertEquals(circle.getArea(),19.63);
        assertEquals(rectangle.getArea(),6.0);
    }
    @Test
    public void shouldGetPerimeter()
    {   assertEquals(rectangle.getPerimeter(),10.0);
        assertEquals(circle.getPerimeter(),15.70,2);

    }
    @Test
    public void shouldSet()
    {
        circle.setRadius(29.3);
        assertEquals(circle.getRadius(),29.3);
        rectangle.setLength(5);
        rectangle.setWidth(12);
        assertEquals(rectangle.getLength(),5.0);
        assertEquals(rectangle.getWidth(),12.0);
        square.setSide(10);
        assertEquals(square.getSide(),10.0);

    }
}
