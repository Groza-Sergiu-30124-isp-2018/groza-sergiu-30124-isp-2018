package g30124.groza.sergiu.l4.ex6;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;

import g30124.groza.sergiu.l4.ex3.Author;

public class BookTest
{
    private Author a1,a2;
    private Author[] a;
    private Book b1;
    @Before
    public void setUp()
    {    a1=new Author("Groza Sergiu","sergiugroza@yahoo.com",'m');
        a2=new Author("Groza Andrei","andreigroza@yahoo.com",'m');
        a=new Author[]{a1,a2};
        b1=new Book ("Java",a,74.9);
    }
    @Test
    public void shouldGetName()
    {
        assertEquals(b1.getName(),"Java");
    }
    @Test
    public void shouldGetPrice()
    {
        assertEquals(b1.getPrice(),74.9);
        b1.setPrice(70);
        assertEquals(b1.getPrice(),70.0);
    }
    @Test
    public void shouldPrintAuthors()
    {
        b1.printAuthors();
    }
}
