package g30124.groza.sergiu.l4.ex5;

import g30124.groza.sergiu.l4.ex3.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock=0;

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {

        return "Book " +
                "name=" + name + ' ' +author.toString();
    }

}
