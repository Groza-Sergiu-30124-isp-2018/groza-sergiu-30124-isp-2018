package g30124.groza.sergiu.l4.ex5;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;

import g30124.groza.sergiu.l4.ex3.Author;

public class BookTest
{
    Book book;
    Author author;
    @Before
    public void setUp()
    { author=new Author("sergiu","sergiugroza@yahoo.com",'m');
        book=new Book("Java",author,3.4);
    }
    @Test
    public void testName()
    {
        assertEquals(book.getAuthor(),"sergiu");

    }
    @Test
    public void getQty()
    {
        book.setQtyInStock(10);
        assertEquals(book.getQtyInStock(),10);

    }
    @Test
    public void shouldPrintInfo()
    {
        assertEquals("Book name=sergiu Author sergiu (m) at sergiugroza@yahoo.com",book.toString());
    }
}
