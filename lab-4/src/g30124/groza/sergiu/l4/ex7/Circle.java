package g30124.groza.sergiu.l4.ex7;

public class Circle
{

    private double radius;
    private String color;

    public Circle()
    {
        radius=1.0;
        color="red";
    }

    public Circle(double radius)
    {
        this.radius=radius;
    }

    double getRadius()
    {
        return radius;
    }

    double getArea()
    {
        return 2*3.14*radius;
    }

    public String toString() {
        return "Circle radius "+ radius +
                ", color=" + color ;
    }

}
