package g30124.groza.sergiu.l4.ex7;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CylinderTest
{

    Circle circle;
    Cylinder cylinder1,cylinder2;

    @Before
    public void setUp()
    {
        cylinder1=new Cylinder(2.5,4);
        circle=new Circle(2.5);
        cylinder2=new Cylinder();
    }
    @Test
    public void shouldGetRadius()
    {
        assertEquals(circle.getRadius(),2.5);
        assertEquals(cylinder2.getRadius(),1.0); // implicit constructor
    }
    @Test
    public void shouldGetArea()
    {
        assertEquals(circle.getArea(),19.63);
        assertEquals(cylinder1.getArea(),102.09,2);
    }
    @Test
    public void shouldPrintInfos()
    {
        assertEquals(circle.toString(),"Circle radius 2.5, color=red");
    }
    @Test
    public void shouldGetVolume()
    {
        assertEquals(cylinder1.getVolume(),78.52);
    }
    @Test
    public void shouldGetHeight()
    {
        assertEquals(cylinder1.getHeight(),4.0);
    }

}
