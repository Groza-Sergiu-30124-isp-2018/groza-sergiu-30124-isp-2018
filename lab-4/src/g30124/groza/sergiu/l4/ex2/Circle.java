package g30124.groza.sergiu.l4.ex2;

public class Circle
{
    private double radius;
    private String color;

    public Circle()
    {
        radius=1.0;
        color="red";
    }

    public Circle(double radius,String color)
    {
        this.radius=radius;
        this.color=color;
    }

    double getRadius()
    {
        return radius;
    }

    double getArea()
    {
        return 2*3.14*radius;
    }

    public static void main(String[] args)
    {
        Circle c=new Circle();
        System.out.println(c.getArea());
        System.out.println(c.getRadius());
    }

}
