package g30124.groza.sergiu.l4.ex2;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCircle
{
    @Test
    public void buildCircle()
    {
        Circle c1= new Circle();
        assertEquals(c1.getArea(),c1.getRadius()*3.14*2);
    }
}
