package g30124.groza.sergiu.l4.ex3;

public class Author
{
    private String name,email;
    private char gender;

    public Author(String name,String email,char gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;
    }

    public String getName()
    {
        return name;
    }

    public String getEmail()
    {
        return email;
    }

    public char getGender()
    {
        return gender;
    }

    public void setEmail(String email)
    {
        this.email=email;
    }

    public String toString()
    {
        return "author-"+getName()+" ("+getGender()+") at "+getEmail();
    }

    public static void main (String[] args)
    {
        Author a= new Author("Sergiu","sergiugroza@yahoo.com",'m');
        a.getEmail();
        a.getName();
    }
}
