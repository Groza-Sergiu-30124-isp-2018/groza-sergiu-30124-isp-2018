package g30124.groza.sergiu.l4.ex3;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class AuthorTest {

    private Author author;
    @Before
    public void setUp(){
        author=new Author("Groza Sergiu","sergiugroza@yahoo.com",'m');

    }
    @Test
    public void setEmailTest()
    {
        author.setEmail("sergiugroza@yahoo.com");
        assertEquals(author.getEmail(),"sergiugroza@yahoo.com");
    }
    @Test
    public void getGenderTest()
    {
        assertEquals(author.getGender(),'m');
    }
    @Test
    public void getNameTest()
    {
        assertEquals(author.getName(),"Groza Sergiu");
    }
    @Test
    public void stringMethod()
    {
        Author a3=new Author("Pop","popioan@yahoo.com",'m');
        assertEquals(a3.toString(),"author-Pop (m) at popioan@yahoo.com");
    }
}
