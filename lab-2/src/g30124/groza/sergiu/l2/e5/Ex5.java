package g30124.groza.sergiu.l2.e5;

import java.util.Scanner;
public class Ex5
{
    public static void main(String[] arg)
    {
        Scanner in = new Scanner(System.in);
        int n= in.nextInt();
        int[] v;
        v=new int[n];
        int i,aux;

        for(i=0;i<n;i++)
            v[i]=in.nextInt();
        int k=0;
        while(k==0)
        {
            k=1;
            for(i=0;i<n-1;i++)
            {
                if(v[i]>v[i+1])
                {
                    aux=v[i];
                    v[i]=v[i+1];
                    v[i+1]=aux;
                    k=0;
                }

            }
        }


        for(i=0;i<n;i++)
            System.out.print(" "+v[i]);


    }
}
