package g30124.groza.sergiu.l10.ex3;

public class Counter1 extends Thread{
    Thread t;
    Counter1(String name,Thread t){
        super(name);
        this.t=t;
    }

    public void run(){
        for(int i=0;i<100;i++){
            System.out.println(getName() + " i = "+i);
            try {if(t!=null)
                t.join();
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
