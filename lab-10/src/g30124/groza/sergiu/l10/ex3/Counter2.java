package g30124.groza.sergiu.l10.ex3;

public class Counter2 extends Thread{
    Thread t;
    Counter2(String name,Thread t){
        super(name);
        this.t=t;
    }

    public void run(){
        for(int i=100;i<=200;i++){

            try {if(t!=null)
                t.join();
                System.out.println(getName() + " i = "+i);
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
