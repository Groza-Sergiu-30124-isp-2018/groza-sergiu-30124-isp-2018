package g30124.groza.sergiu.l10.ex4;

public class Robot extends Thread {
    public int x;
    public int y;
    static Robot[][] matrix = new Robot[100][100];
    boolean active;
    String name;

    Robot(String name, boolean active, int x, int y) {
        this.name = name;
        this.active = active;
        this.x = x;
        this.y = y;
    }

    Robot(String name) {
        int a, b;
        active = false;

        while (active == false) {
            a = generateNumber(100);
            b = generateNumber(100);
            if (checkValid(a, b) == true) {
                active = true;
                this.x = a;
                this.y = b;
                matrix[this.x][this.y] = new Robot(name, active, this.x, this.y);
                matrix[this.x][this.y].start();
            }

        }
    }

    @Override
    public String toString() {
        return "Robot(" + x +","+ y + ")name='" + name + '\'' +
                '}';
    }

    public int generateNumber(int n) {
        return (int) (Math.random() * n);
    }

    public boolean checkValid(int x, int y) {
        if (matrix[x][y] == null)
            return true;
        else
            return false;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void run() {
        int old_coord;

        while (isActive() == true) {
            try {
                System.out.println("Robotul " + name + " se afla pe (" + x + "," + y + ")");
                int direction = generateNumber(4);
                switch (direction) {
                    case 0:
                        if (x < 99) {
                            old_coord = x;
                            x++;
                            if (!checkIfFree(old_coord, y, x, y)) {
                                matrix[x][y] = matrix[old_coord][y];
                                matrix[old_coord][y] = null;
                            } else {
                                System.out.println("-----------");
                                System.out.println("Crash " + matrix[x][y].toString() + " and " + matrix[old_coord][y].toString());
                                matrix[x][y].setActive(false);
                                matrix[old_coord][y].setActive(false);
                            }
                        }
                        break;
                    case 1:
                        if (y < 99) {
                            old_coord = y;
                            y++;
                            if (!checkIfFree(x, old_coord, x, y)) {
                                matrix[x][y] = matrix[x][old_coord];
                                matrix[x][old_coord] = null;
                            } else {
                                System.out.println("-----------");
                                System.out.println("Crash " + matrix[x][y].toString() + " and " + matrix[x][old_coord].toString());
                                matrix[x][y].setActive(false);
                                matrix[x][old_coord].setActive(false);
                            }
                        }
                        break;
                    case 2:
                        if (x > 1) {
                            old_coord = x;
                            x--;
                            if (!checkIfFree(old_coord, y, x, y)) {
                                matrix[x][y] = matrix[old_coord][y];
                                matrix[old_coord][y] = null;
                            } else {
                                System.out.println("-----------");
                                System.out.println("Crash " + matrix[x][y].toString() + " and " + matrix[old_coord][x].toString());
                                matrix[x][y].setActive(false);
                                matrix[old_coord][y].setActive(false);
                            }
                        }
                        break;
                    case 3:
                        if (y > 1) {
                            old_coord = y;
                            y--;
                            if (!checkIfFree(x, old_coord, x, y)) {
                                matrix[x][y] = matrix[x][old_coord];
                                matrix[x][old_coord] = null;
                            } else {
                                System.out.println("-----------");
                                System.out.println("Crash " + matrix[x][y].toString() + " and " + matrix[x][old_coord].toString());
                                matrix[x][y].setActive(false);
                                matrix[x][old_coord].setActive(false);
                            }
                        }
                        break;

                }

                Thread.sleep(100);

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isActive())
                return;
        }
    }

    public boolean isActive() {
        return active;
    }

    public boolean checkIfFree(int x1, int y1, int x2, int y2) {
        if (matrix[x1][y1] != null && matrix[x2][y2] != null)
            return true;
        else
            return false;

    }
    public static void main(String [] args)
    {

        new Robot("1");
        new Robot("2");
        new Robot("3");
        new Robot("4");
        new Robot("5");
        new Robot("6");
        new Robot("7");
        new Robot("8");
        new Robot("9");
        new Robot("10");



    }
}
