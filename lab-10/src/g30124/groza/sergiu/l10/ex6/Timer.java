package g30124.groza.sergiu.l10.ex6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Timer extends JFrame{
    JTextArea tArea;
    JLabel counter;
    JTextField tcounter;
    JButton startb,reset;

    public int millis=0,seconds=0;
    boolean mod;


    public boolean isMod() {
        return mod;
    }


    Timer(){

        setTitle("Cronometru");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        mod=false;
        this.setLayout(null);
        int width=80;int height = 20;

        counter = new JLabel("Count:");
        counter.setBounds(10,50,width,height);

        tcounter = new JTextField();
        tcounter.setBounds(60,50,width, height);

        startb = new JButton("Start/Stop");
        startb.setBounds(10,100,width, height);

        reset = new JButton("Reset");
        reset.setBounds(110,100,width, height);

        startb.addActionListener(new TratareButonStart());
        reset.addActionListener(new TratareButonReset());

        add(tcounter);add(startb);add(reset);add(counter);

        Timer.this.tcounter.setText(seconds+":"+millis);
    }

    class TimerThread extends Thread{

        public void run()
        {
            while(mod==true)
            {
                try
                {
                Thread.sleep(1);
                   if(millis>=1000)
                   {
                       millis=0;
                       seconds++;
                   }

                    millis++;
                    Timer.this.tcounter.setText(seconds+":"+ millis);
                }
                catch(Exception e)
                {

                }
            }


        }
    }

    class TratareButonStart implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            mod=!mod;
            TimerThread t=new TimerThread();
            t.start();
        }
    }

    class TratareButonReset implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            mod=false;
            millis=0;
            seconds=0;
            Timer.this.tcounter.setText("0:0");
        }
    }
public static void main(String [] args)
{
    Timer time=new Timer();
}


}
